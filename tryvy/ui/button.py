#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.lang import Builder

from kivymd.uix.button import MDRaisedButton

Builder.load_string('''
<TrytonRaisedButton>
    elevation_normal: 2
    opposite_colors: True
    pos_hint: {'center_x': 0.5, 'center_y': 0.4}
''')


class TrytonRaisedButton(MDRaisedButton):
    pass
