#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from kivy.lang import Builder
from kivy.metrics import dp
from kivy.properties import (ListProperty, ObjectProperty, StringProperty,
    NumericProperty, OptionProperty, ColorProperty)
from kivy.uix.modalview import ModalView
from kivy.animation import Animation
from kivy.uix.boxlayout import BoxLayout
from kivy.event import EventDispatcher

from kivymd.theming import ThemableBehavior
from kivymd.uix.behaviors import RectangularElevationBehavior
from kivymd.uix.list import OneLineListItem
from kivymd.uix.label import MDLabel
from ..filler_init import FillerInit

from tryvy.misc import format_date
from kivymd.uix.picker import MDDatePicker
import gettext

_ = gettext.gettext

Builder.load_string('''
#:import m_res kivymd.material_resources
<Filterbar>
    canvas:
        Color:
            rgba:     self.theme_cls.bg_light
        Rectangle:
            size:     root.toolbar.size
            pos:      root.toolbar.pos
    size_hint: 1, None
    height: root.theme_cls.standard_increment
    padding: [0, 0]
    opposite_colors: False
    elevation: 12
    BoxLayout:
        pos: self.parent.pos
        width: dp(48)
        MDIconButton:
            icon: 'arrow-left'
            on_release: root.on_close(force=True)
    BoxLayout:
        width: dp(48)
        pos: self.parent.pos
        padding: self.width - dp(50), 0
        MDIconButton:
            icon: 'close'
            on_release: root.on_close(force=False)

<FilterText>
    padding: dp(50), 0
    MDTextField:
        id: filter_text
        on_text_validate: root.on_validate(self.text)

<FilterResultItem>
    font_style: 'Subtitle1'
''')


class FilterMixin(object):

    def on_close(self, force):
        self.parent.dismiss()
        self.parent.on_filter_record(None)

    def on_open(self):
        pass

    def on_validate(self, value):
        self.parent.on_filter_record(value)


class FilterText(BoxLayout, FilterMixin):

    def on_close(self, force):
        if self.ids.filter_text.text and not force:
            self.ids.filter_text.text = ''
        else:
            super().on_close(force)

    def on_open(self):
        self.ids.filter_text.focus = True


class FilterDatePicker(MDDatePicker):
    background_color = ListProperty([0, 0, 0, .3])


class FilterbarMixin(FillerInit):
    toolbar = ObjectProperty(None)
    owner = ObjectProperty(None)
    result_info = ObjectProperty(None)
    filter_fields = []
    operator = ''
    value = ObjectProperty(None, allownone=True)
    prefilter_fields = []
    prefilter_value = ObjectProperty(None, allownone=True)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        assert kwargs.get('filters') or self.filter_fields
        if kwargs.get('filters'):
            self.filter_fields = kwargs.get('filters')
        if kwargs.get('filter_operator'):
            self.operator = kwargs['filter_operator']
        self.prefilter_fields = kwargs.get('prefilters', [])

    def get_toolbar_actions(self):
        pass

    def open(self):
        pass

    def dismiss(self):
        pass

    def on_filter_record(self, value, prefilter=False):
        # add result item
        if prefilter:
            self.prefilter_value = value
            self.value = None
        else:
            self.value = value
            self.prefilter_value = None
        if value and not self.result_info:
            self.result_info = FilterResultItem()
        self.owner.on_filter_record(
            self._get_filter_domain(), self.result_info)

    def _get_filter_domain(self):
        pass


class FilterbarDate(EventDispatcher, FilterbarMixin):
    picker = None
    filter_fields = ['effective_date']
    _open = False
    _date_mark = None
    operator = '='

    def __init__(self, **kwargs):
        super(FilterbarDate, self).__init__(**kwargs)
        self.value = datetime.datetime.today()

    def get_toolbar_actions(self):
        return [
            ['magnify', lambda x: self.owner.on_init_filter()],
            ['calendar', lambda x: self.on_filter_record(
                datetime.datetime.today())]]

    def open(self):
        if not self.picker:
            # TODO: load filtered date
            self.picker = FilterDatePicker(
                title=_('SELECT DATE'),
                title_input=_('INPUT DATE'))
        self.picker.bind(on_save=self.on_picker_save)
        if self.picker._window:
            self.picker.dismiss()
            return
        self.picker.open()

    def dismiss(self):
        if self.toolbar.children[1].children:
            self.toolbar.children[1].remove_widget(
                self.toolbar.children[1].children[0])
        if self.picker:
            self.picker.dismiss()

    def on_picker_save(self, instance, value, date_range):
        self.on_filter_record(value)

    def on_filter_record(self, value):
        super().on_filter_record(value)
        _label = None
        if len(self.toolbar.children[1].children) == 1:
            _label = MDLabel(
                font_style='Subtitle1',
                opposite_colors=self.toolbar.opposite_colors,
                theme_text_color='Custom',
                text_color=self.toolbar.specific_text_color,
                halign='right')
            self.toolbar.children[1].add_widget(_label)
        elif self.toolbar.children[1].children:
            _label = self.toolbar.children[1].children[0]
        if _label:
            date = self.value
            if isinstance(self.value, datetime.datetime):
                date = self.value.date()
            _label.text = format_date(date)

    def _get_filter_domain(self):
        if not self.value:
            return []
        res = []
        for _field in self.filter_fields:
            if self.owner.tryton_view.model_fields[_field].get(
                    'ttype') == 'datetime':
                operators = {
                    '=': ('>=', '<='),
                    '>=': ('>=', None),
                    '<=': (None, '<='),
                    '>': (None, '>'),
                    '<': ('<', None),
                }
                op1, op2 = operators[self.operator]
                if op1:
                    res.append((_field, op1,
                        self.value.strftime('%Y-%m-%d 00:00:00')))
                if op2:
                    res.append((_field, op2,
                        self.value.strftime('%Y-%m-%d 23:59:59')))
            else:
                res.append((_field, self.operator,
                    self.value.strftime('%Y-%m-%d')))
        return res


class Filterbar(ThemableBehavior, RectangularElevationBehavior, ModalView,
                FilterbarMixin):
    overlay_color = ColorProperty([0, 0, 0, 0])
    filter_ = ObjectProperty(None)
    type_ = OptionProperty('Text', options=['Text'])
    _types = {
        'Text': FilterText,
    }
    operator = 'ilike'
    filter_fields = ListProperty(['rec_name'])

    def __init__(self, **kwargs):
        super(Filterbar, self).__init__(**kwargs)
        self.filter_ = self._types[self.type_]()
        self.add_widget(self.filter_, index=1)

    def get_toolbar_actions(self):
        return [['magnify', lambda x: self.owner.on_init_filter()]]

    def on_close(self, force):
        self.filter_.on_close(force)

    def open(self, *largs):
        if self._window is not None:
            return self
        # search window
        super().open(*largs)
        if not self._window:
            return self
        self.center = self.toolbar.center
        self.size = self.toolbar.size
        self.pos = self.toolbar.pos
        self.bind(size=self._align_center)
        self.filter_.on_open()
        return self

    def _align_center(self, *_args):
        if self._is_open:
            self.center = self.toolbar.center

    def _get_filter_domain(self):
        value = None
        if self.prefilter_value:
            value = self.prefilter_value
            operator = self.prefilter_operator
            fields = self.prefilter_fields
        elif self.value:
            value = self.value
            operator = self.operator
            fields = self.filter_fields
        if not value:
            return []
        filter_value = '%' + value + '%' if 'like' in operator else value
        return [(_field, operator, filter_value)
            for _field in fields]

    def on_touch_down(self, touch):
        if not self.collide_point(*touch.pos):
            # check if touching float button
            if self.owner.manager.root_layout.ids.float_btn.collide_point(
                    *touch.pos):
                self.dismiss()
                self.owner.manager.root_layout.on_press_float_btn()
            else:
                self.owner.on_touch_down(touch)
            return True
        super().on_touch_down(touch)
        return True

    def on_touch_move(self, touch):
        if not self.collide_point(*touch.pos):
            self.owner.on_touch_move(touch)
            return True
        super().on_touch_move(touch)
        return True

    def dismiss(self, *largs, **kwargs):
        if self.result_info and self.result_info.parent:
            self.result_info.parent.remove_widget(self.result_info)
        super(Filterbar, self).dismiss(*largs, **kwargs)


class FilterResultItem(OneLineListItem):
    counter = NumericProperty(0)
    info = StringProperty('')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._txt_top_pad = dp(5)
        self.txt_bot_pad = dp(5)
        self.height = dp(30)

    def on_counter(self, instance, value):
        self.counter = value
        self.text = ('%s  ' % self.info if self.info else ''
            ) + ('%s            %s' % (_('RESULTS'), value))


class FilterbarFactory(object):
    _types = {
        None: None,
        '': None,
        'Text': Filterbar,
        'Date': FilterbarDate,
    }

    @classmethod
    def create_filter_bar(cls, bar_type, filter_fields, **kwargs):
        cls_filter = cls._types[bar_type]
        if cls_filter:
            kwargs['filters'] = filter_fields
            return cls_filter(**kwargs)
        return None
