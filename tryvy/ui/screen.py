#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from functools import partial
from kivy.app import App
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.properties import (ObjectProperty, StringProperty, BooleanProperty,
    AliasProperty, ListProperty, NumericProperty, DictProperty)

from kivymd.utils import asynckivy
from kivymd.effects.stiffscroll import StiffScrollEffect
from kivymd.uix.tab import MDTabsBase
from kivy.uix.floatlayout import FloatLayout

from .tree import TrytonTreeMixin
from .form import TrytonFormMixin, TrytonTabbedForm
from .filterbar import FilterbarFactory
from .dialog import ErrorDialog
import gettext
from tryvy.ui.view.field import FieldMixin
from kivy.core.window import Window
from tryvy.filler_init import FillerInit

_ = partial(gettext.dgettext, 'tryvy')
MINIMUM_WIDTH_SCREEN = 720


Builder.load_string('''
#:import TrytonToolbar tryvy.ui.toolbar.TrytonToolbar

<TrytonScreenManager>
    HomeScreen:

<TrytonScreen>
    BoxLayout:
        orientation: "vertical"

        TrytonToolbar:
            id: toolbar
            title: root.title

        FloatLayout:
            id: screen_container
''')


class TrytonScreenManager(ScreenManager):

    def equal_contexts(self, context, other_context):
        return set(context.values()) & set(other_context.values())

    def on_go_to(self, screen, record=None, check_logged=False,
            title=None, context=None):
        if check_logged and not self.parent.root_app.logged:
            dialog = ErrorDialog()
            dialog.open(title=_('Session expired!'),
                text=_('Must log in application'))
            return
        assert screen
        new_screen_name = screen
        if isinstance(screen, Screen):
            new_screen_name = screen.name
        if new_screen_name == self.current_screen.name:
            if (not context
                    or not self.current_screen.context
                    or self.equal_contexts(context,
                        self.current_screen.context)):
                return
        self.current_screen.on_pre_out()
        self.root_layout.configure_float_btn(None)
        self.transition.direction = 'left'
        if context or title:
            nextsc = self.get_screen(new_screen_name)
            nextsc.title = title
            if context:
                nextsc.context.update(context)
            self.current_screen.on_pre_enter()
        if isinstance(screen, Screen):
            self.current = screen.name
        else:
            self.current = screen

        if record is not None:
            nextsc = self.get_screen(new_screen_name)
            if (issubclass(nextsc.__class__, TrytonScreen) and
                    nextsc.tryton_view and
                    hasattr(nextsc.tryton_view, 'record')):
                nextsc.tryton_view.record = record

    def _get_default_toolbar_actions(self, screen):
        if screen.prev_screen:
            left_actions = [['close',
                lambda x: screen.on_go_back()]]
        else:
            left_actions = [['menu',
                lambda x: self.parent.nav_drawer.set_state("toggle")]]
        return left_actions, []

    @property
    def root_layout(self):
        return self.parent.parent


class TrytonScreen(FillerInit, Screen):
    with_prev = BooleanProperty(False)
    prev_screen = StringProperty('')
    next_screen = StringProperty('')
    new_record_screen = StringProperty('')
    title = StringProperty('')
    tryton_view = ObjectProperty(None)
    context = DictProperty('')
    go_back = BooleanProperty(True)

    def __init__(self, **kwargs):
        super(TrytonScreen, self).__init__(**kwargs)
        self.register_event_type('on_go_back')

    def on_go_back(self):
        assert self.prev_screen
        self.on_pre_out()
        self.manager.root_layout.configure_float_btn(None)
        self.manager.transition.direction = 'right'
        self.manager.current = self.prev_screen

    def save_record(self):
        record = self.tryton_view.record
        res = self.tryton_view.record['id'].value
        if record.modified:
            res = self.tryton_view.save()
        if self.go_back and res:
            self.on_go_back()

    def on_go_forward(self, record=None):
        assert self.next_screen
        self.on_pre_out()
        self.manager.root_layout.configure_float_btn(None)
        self.manager.transition.direction = 'left'
        self.manager.current = self.next_screen
        if record is not None:
            nextsc = self.manager.get_screen(self.next_screen)
            if (issubclass(nextsc.__class__, TrytonScreen) and
                    nextsc.tryton_view and
                    hasattr(nextsc.tryton_view, 'record')):
                nextsc.tryton_view.record = record
                nextsc.tryton_view.rebind_record()
                nextsc.tryton_view._reload()

    def on_pre_enter(self):
        self.configure_toolbar()
        self.configure_float_button()

    def on_pre_out(self):
        pass

    def get_toolbar_actions(self):
        left, right = self.manager._get_default_toolbar_actions(self)
        if self.tryton_view:
            pos = 0
            if self.tryton_view.perm_modify:
                pos = 1
                if self.go_back:
                    right.insert(0, ['check',
                        lambda x: self.save_record()])
                else:
                    right.insert(0, ['content-save',
                        lambda x: self.save_record()])
                    left[0] = ['arrow-left', lambda x: self.on_go_back()]
            if self.tryton_view.perm_delete:
                right.insert(pos, ['delete',
                    lambda x: self.tryton_view.delete_record()])
        elif not self.go_back:
            left[0] = ['arrow-left', lambda x: self.on_go_back()]
        return left, right

    @property
    def _new_record_screen(self):
        return self.new_record_screen or self.next_screen

    def configure_toolbar(self):
        left_acts, right_acts = self.get_toolbar_actions()
        _toolbar = self.ids.toolbar
        if left_acts:
            _toolbar.left_action_items = left_acts
        if right_acts:
            _toolbar.right_action_items = right_acts
        _toolbar.title = self.title

    def configure_float_button(self, icon=None):
        pass


class TrytonTreeScreen(TrytonScreen):
    filter_bar = ObjectProperty(None)
    filter_fields = ListProperty([])
    filter_operator = StringProperty(None, allownone=True)
    _filterbar_type = StringProperty('Text', allownone=True)
    prefilter_fields = ListProperty([])
    prefilter_operator = StringProperty(None, allownone=True)

    def __init__(self, **kwargs):
        super(TrytonTreeScreen, self).__init__(**kwargs)
        self.reload_on_enter = kwargs.get('reload_on_enter', True)
        if self.filterbar_type:
            Clock.schedule_once(self._create_filterbar, 0)

    def _get_filterbar_type(self):
        return self._filterbar_type

    def _set_filterbar_type(self, value):
        self._filterbar_type = value

    def _create_filterbar(self, dt=None):
        if not self.filter_bar:
            self.filter_bar = FilterbarFactory.create_filter_bar(
                self.filterbar_type, self.filter_fields,
                auto_dismiss=False, toolbar=self.ids.toolbar,
                owner=self, filter_operator=self.filter_operator,
                prefilters=self.prefilter_fields,
                prefilter_operator=self.prefilter_operator)
        return self.filter_bar

    filterbar_type = AliasProperty(_get_filterbar_type, _set_filterbar_type,
        bind=['_filterbar_type'])

    def add_widget(self, widget, **kwargs):
        if isinstance(widget, TrytonTreeMixin):
            self.tryton_view = widget
            widget.root_layout = self
            self.ids.screen_container.add_widget(widget)
        else:
            super(TrytonTreeScreen, self).add_widget(widget)

    def on_pre_enter(self):
        super(TrytonTreeScreen, self).on_pre_enter()
        if self.filter_bar and self.filter_bar.value:
            self.filter_bar.on_filter_record(self.filter_bar.value)
        elif self.tryton_view and self.reload_on_enter:
            self.tryton_view._extra_pages = 0
            self.tryton_view._reload()

    def on_pre_out(self):
        self.dismiss_filter_bar()

    def get_toolbar_actions(self):
        left_acts, right_acts = super(TrytonTreeScreen,
            self).get_toolbar_actions()
        self._create_filterbar()
        if self.filter_bar:
            right_acts = self.filter_bar.get_toolbar_actions() + right_acts
        return left_acts, right_acts

    def dismiss_filter_bar(self):
        if self.filter_bar:
            self.filter_bar.dismiss()

    def on_init_filter(self):
        self._create_filterbar()
        self.filter_bar.open()

    def on_filter_record(self, domain, result_info):
        self.tryton_view._clear()
        if (domain and result_info and result_info not in
                self.tryton_view.container.children):
            self.tryton_view.container.add_widget(result_info,
                index=len(self.tryton_view.container.children))
        self.tryton_view._reload(domain, clear=False)
        if domain and result_info:
            result_info.on_counter(result_info, len(self.tryton_view.records))

    def show_record(self, record, context=None):
        root_app = App.get_running_app()
        smanager = root_app.nav_layout.ids.smanager
        if not self.next_screen:
            return
        next_screen = smanager.get_screen(self.next_screen)

        async def show_record_on_next_screen():
            next_screen.tryton_view.show_record(record['id'].value,
                context=context or self.context)
            if isinstance(next_screen.tryton_view, TrytonTabbedForm):
                next_screen.tryton_view.show_main_tab()
            await asynckivy.sleep(0)
            smanager.on_go_to(next_screen.name, context=context)
            if (self.tryton_view._many2one_record and
                    self.tryton_view._many2one_name):
                next_screen.tryton_view.record.add_parent(
                    self.tryton_view._many2one_name,
                    self.tryton_view._many2one_record)
            # to not execute on_change events during loading record
            next_screen.tryton_view.record.state = 'edit'

        asynckivy.start(show_record_on_next_screen())

    def new_record(self, **kwargs):
        root_app = App.get_running_app()
        next_screen = root_app.nav_layout.ids.smanager.get_screen(
            self._new_record_screen)
        next_screen.tryton_view.new_record(tree_view=self.tryton_view,
            **kwargs)
        if isinstance(next_screen.tryton_view, TrytonTabbedForm):
            next_screen.tryton_view.show_main_tab()
        root_app.nav_layout.ids.smanager.on_go_to(next_screen.name)
        next_screen.tryton_view.rebind_record()

    def configure_float_button(self, icon=None):
        self.manager.root_layout.configure_float_btn(
            self._get_float_button_callback(), icon=icon)

    def _get_float_button_callback(self):
        callback = None
        if self.tryton_view and self.tryton_view.perm_create:
            callback = self.tryton_view.new_record
        return callback


class TrytonFormScreen(TrytonScreen):
    auto_save = BooleanProperty(False)

    def get_title(self, title_name, title_code):
        if Window.width > MINIMUM_WIDTH_SCREEN:
            return '{} {}'.format(title_name, title_code)
        else:
            return '{}'.format(title_code)

    def add_widget(self, widget, **kwargs):
        if isinstance(widget, TrytonFormMixin):
            self.tryton_view = widget
            self.ids.screen_container.add_widget(widget, **kwargs)
        else:
            super(TrytonFormScreen, self).add_widget(widget, **kwargs)

    def on_pre_enter(self):
        super(TrytonFormScreen, self).on_pre_enter()
        if self.tryton_view:
            self.tryton_view._extra_pages = 0
            self.reset_counter_fields()
            if not self.tryton_view.record.modified:
                self.tryton_view._reload()
            if isinstance(self.tryton_view, TrytonTabbedForm):
                curr_screen = self.tryton_view.get_current_tab()
                if curr_screen:
                    curr_screen.on_pre_enter()

    def on_go_back(self):
        super().on_go_back()
        self.tryton_view.clear_record()

    def reset_counter_fields(self):
        for child in self.children:
            for item in child.children:
                for field in item.children:
                    if isinstance(field, FieldMixin):
                        field.modified = False

    def configure_float_button(self, icon=None):
        self.manager.root_layout.configure_float_btn(
            self._get_float_button_callback(), icon=icon)

    def _get_float_button_callback(self):
        callback = None
        if self.tryton_view and self.tryton_view.perm_create:
            callback = self.tryton_view.new_record
        return callback


class TrytonTabScreen(FloatLayout, MDTabsBase):
    name = StringProperty('')
    tryton_view = ObjectProperty(None)
    counter = NumericProperty(0)
    hide_counter = BooleanProperty(False)
    next_screen = StringProperty('')
    new_record_screen = StringProperty('')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.bind(counter=self._update_text)

    def add_widget(self, widget, **kwargs):
        if isinstance(widget, (TrytonFormMixin, TrytonTreeMixin)):
            self.tryton_view = widget
            # do not limit records as swipe actions does not work on tab
            self.tryton_view.effect_cls = StiffScrollEffect
            self.tryton_view._limit = None
        super(TrytonTabScreen, self).add_widget(widget, **kwargs)

    def on_pre_enter(self):
        if self.tryton_view:
            record = getattr(self.tryton_view, 'record', None)
            if (record and not self.tryton_view.record.modified) or not record:
                self.tryton_view._reload()
        self.configure_float_button()

    def _get_float_button_callback(self):
        callback = None
        if self.tryton_view and self.tryton_view.perm_create:
            callback = self.tryton_view.new_record
        return callback

    def configure_float_button(self, icon=None):
        create_callback = self._get_float_button_callback()
        if self.tryton_view:
            self.tryton_view.app.root_layout.configure_float_btn(
                create_callback)

    @property
    def tryton_screen(self):
        parent = self.parent
        while parent:
            if isinstance(parent, TrytonScreen):
                break
            parent = parent.parent
        return parent

    def _get_context(self):
        return self.tryton_screen.context

    context = AliasProperty(_get_context)

    def show_record(self, record, context=None):
        root_app = App.get_running_app()
        next_screen = root_app.nav_layout.ids.smanager.get_screen(
            self.next_screen)
        next_screen.tryton_view.show_record(
            record if isinstance(record, int) else record['id'].value,
            context=context or self.context)
        root_app.nav_layout.ids.smanager.on_go_to(next_screen.name,
            context=context)
        if (self.tryton_view._many2one_record
                and self.tryton_view._many2one_name):
            next_screen.tryton_view.record.add_parent(
                self.tryton_view._many2one_name,
                self.tryton_view._many2one_record)
        # to not execute on_change events during loading record
        next_screen.tryton_view.record.state = 'edit'

    @property
    def _new_record_screen(self):
        return self.new_record_screen or self.next_screen

    def new_record(self, **kwargs):
        root_app = App.get_running_app()
        next_screen = root_app.nav_layout.ids.smanager.get_screen(
            self._new_record_screen)
        if (self.tryton_view._many2one_record
                and self.tryton_view._many2one_name
                and self.tryton_screen):
            # save master record
            self.tryton_screen.tryton_view.save()
        next_screen.tryton_view.new_record(tree_view=self.tryton_view,
            **kwargs)
        root_app.nav_layout.ids.smanager.on_go_to(next_screen.name)
        next_screen.tryton_view.rebind_record()

    def clear_record(self):
        self.tryton_view.clear_record()

    def configure_toolbar(self):
        if self.tryton_screen:
            self.tryton_screen.configure_toolbar()

    def _update_text(self, *args):
        """Adds counter on tab label"""
        super()._update_text(*args)
        if self.hide_counter:
            return
        text = ['(%s)' % self.counter]
        if self.tab_label_text:
            text.insert(0, self.tab_label_text)
        text = ' '.join(text)
        self.tab_label_text = text
