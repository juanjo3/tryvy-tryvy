#!/usr/bin/env pythonMany2One
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivymd.app import MDApp
from kivy.lang import Builder
from tryvy.ui.tree import TreeAvatarCheckItem
from tryvy.ui.tree import TrytonTree, TreeItemFactory
from .many2one import Many2OneMixin
root_app = MDApp.get_running_app()


Builder.load_string('''
<Many2Many>
    Many2ManyTree

<Many2ManyItem>:
    text: self.record['rec_name'].value.splitlines()[0]
    secondary_text: ' '.join(self.record['rec_name'].value.splitlines()[1:]) if len(self.record['rec_name'].value.splitlines()) > 1 else ''
''')


class Many2ManyItem(TreeAvatarCheckItem):

    def show_record(self, context=None):
        # it does nothing when item is clicked
        pass


class Many2ManyTree(TrytonTree):
    _limit = 20
    _fields = ['rec_name']
    item_cls = Many2ManyItem

    def activate_records(self, record_id, value):
        super().activate_records(record_id, value)
        if self._active_records:
            root_app.root_layout.configure_float_btn(
                self.add_records, icon='account-multiple-plus')
        else:
            root_app.root_layout.configure_float_btn(None)

    def add_records(self):
        self.tryton_screen.set_values(self._active_records)

    def show_record(self, record, context=None):
        self.tryton_screen.set_value(record)
        if self.tryton_screen.go_back_on_set:
            self.tryton_screen.on_go_back()

    def get_model_name(self):
        return self.tryton_screen.model_name

    def _get_record_domain(self):
        res = super()._get_record_domain()
        if self.tryton_screen.relation_domain:
            res.extend(self.tryton_screen.relation_domain)
        return res


class Many2Many(Many2OneMixin):

    def set_value(self, new_value):
        pass

    def set_values(self, new_values):
        self.values = new_values
        for new_value in new_values:
            self._set_value(new_value)
        self.dispatch('on_set_value')
        self.show_records()

    def show_records(self):
        super().on_go_back()

    def on_set_value(self, *args):
        pass


TreeItemFactory().register({
    Many2ManyTree.__name__: Many2ManyItem
    })
