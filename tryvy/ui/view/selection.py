#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.properties import ObjectProperty
from kivymd.uix.list import OneLineListItem


class Selection(OneLineListItem):
    record = ObjectProperty(None)
    callback = ObjectProperty()

    def on_release(self):
        super(Selection, self).on_release()
        if self.callback:
            self.callback(self, self.record)
