#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.lang import Builder
from kivymd.uix.toolbar import MDToolbar

Builder.load_string('''
<TrytonToolbar>
    md_bg_color: app.theme_cls.primary_color
    background_palette: 'Primary'
    background_hue: '500'
''')


class TrytonToolbar(MDToolbar):
    pass
