#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import sys
import traceback
from kivy.metrics import dp
from kivy.lang import Builder
from kivy.properties import (ObjectProperty, AliasProperty, NumericProperty)
from kivy.uix.scrollview import ScrollView
from kivy.uix.gridlayout import GridLayout
from kivy.effects.scroll import ScrollEffect
from kivymd.uix.tab import MDTabs, MDTabsBase

from .dialog import ConfirmDialog
from ..common.common import ModelAccess
from ..tryton.call import execute as call_execute
from .model.record import Record
from .dialog import ErrorDialog
from .context import TrytonContext
import gettext

_ = gettext.gettext


Builder.load_string('''
<FormItem>
    app: app
    do_scroll_x: False
    container: container.__self__
    FormLayout:
        id: container

<FormLayout>
    cols: 1
    adaptive_height: True
    size_hint_y: None
    height: self._min_form_row
    padding: self._form_x_padding, self._form_y_padding

<TrytonTabbedForm>
    app: app
''')


class FormLayout(GridLayout):
    _min_form_row = dp(80)
    _form_y_padding = dp(10)
    _form_x_padding = dp(10)

    def add_widget(self, widget, index=0, canvas=None):
        super(FormLayout, self).add_widget(widget, index)
        self.height += max(widget.height, self._min_form_row)

    def remove_widget(self, widget):
        super(FormLayout, self).remove_widget(widget)
        self.height -= max(widget.height, self._min_form_row)


class TrytonFormMixin(TrytonContext):
    record = ObjectProperty(Record({}), rebind=True)
    app = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(TrytonFormMixin, self).__init__(**kwargs)
        self.register_event_type('on_delete')
        self.register_event_type('on_save')
        self.register_event_type('on_saved')

    def delete_record(self):
        if not self.record:
            return
        dialog = ConfirmDialog()
        dialog.open(title=_('Delete record'),
            text=_('Current record will be deleted. Are you sure?'),
            on_accept=self._delete_record)

    def _delete_record(self, result):
        if not result or not self.record['id'].value:
            return
        try:
            call_execute(self._type, self._name,
                'delete', [self.record['id'].value])
        except Exception as e:
            dialog = ErrorDialog()
            dialog.open(title=_('Cannot delete record.'), text='', exception=e)
            return
        self.record = Record({})
        self.dispatch('on_delete')

    def new_record(self, tree_view=None, **kwargs):
        self.record = Record(self._fields, context=self.context,
            model_name=self._name)
        if tree_view:
            tree_view.update_new_record(self.record)
        for field, value in kwargs.items():
            self.record[field].value = value
            changes = self.record.on_change(field)
            if changes:
                self.record.set_on_change(changes)

    def on_delete(self, *args):
        if not isinstance(self, TrytonTabbedForm):
            self.refresh()

    def _reload(self):
        if self.record and self.record['id'].value and \
                int(self.record['id'].value) > 0:
            self.record.read()
            self.rebind_record()

    def update_record(self, name, value=None):
        assert (isinstance(name, str) and value) or (
            isinstance(name, dict) and value is None)

        if isinstance(name, str):
            self.record[name] = value
        elif isinstance(name, dict):
            self.record.update(name)
        self.rebind_record()

    def rebind_record(self):
        # Force rebind view
        prop = self.property('record')
        prop.dispatch(self)

    def save(self):
        try:
            res = self.record.save()
        except Exception as e:
            dialog = ErrorDialog()
            dialog.open(title=_('Error saving record.'),
                text='',
                exception=e)
            return
        self.dispatch('on_save')
        return res

    def on_save(self, *args):
        self.refresh()
        self._reload()
        self.dispatch('on_saved')

    def on_saved(self, *args):
        pass

    def refresh(self):
        pass

    def show_record(self, record_id, context=None):
        try:
            item, = call_execute(self._type, self._name,
                'read', [record_id], self._fields)
            self.record = Record(item, context=context or self.context,
                model_name=self._name)
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            dialog = ErrorDialog()
            dialog.open(title=_('Connection error'),
                text=_('Cannot connect to server! ' +
                     'Please check the connection settings ' +
                     'and device connectivity.'),
                exception=e)
            return

    def clear_record(self):
        self.record = Record([])

    @property
    def tryton_screen(self):
        if isinstance(self.parent, MDTabsBase):
            return self.parent
        return self.parent.parent.parent


class FormItem(ScrollView, ModelAccess, TrytonFormMixin):
    _type = 'model'
    _name = ''
    _fields = []
    view = ObjectProperty(None)
    container = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.effect_cls = ScrollEffect

    def add_widget(self, widget, **kwargs):
        if self.container:
            self.container.add_widget(widget, **kwargs)
        else:
            super(FormItem, self).add_widget(widget, **kwargs)


class TrytonTabbedForm(MDTabs, ModelAccess, TrytonFormMixin):
    main_tab = NumericProperty(0)

    @property
    def _defaul_tab(self):
        return self.default_tab or self.main_tab

    def get_record(self):
        return self.get_tab(
            self.main_tab).tryton_view.record

    def set_record(self, value):
        screen = self.get_tab(self.main_tab)
        screen.tryton_view.record = value

    def rebind_record(self):
        # Force rebind view
        tryton_view = self.get_tab(self.main_tab).tryton_view
        prop = tryton_view.property('record')
        prop.dispatch(tryton_view)

    record = AliasProperty(get_record, set_record, rebind=True)

    def _delete_record(self, result):
        if not result:
            return
        self.get_tab(self.main_tab).tryton_view._delete_record(result)
        self.dispatch('on_delete')

    def show_main_tab(self):
        self.switch_tab(self.get_tab(self.main_tab).name, search_by='name')

    def show_record(self, record_id, context=None):
        self.get_tab(self.main_tab
            ).tryton_view.show_record(record_id, context=context)

    def new_record(self, **kwargs):
        self.get_tab(self.main_tab).tryton_view.new_record(**kwargs)

    def clear_record(self):
        self.get_tab(self.main_tab).clear_record()

    def get_tab(self, tab_index):
        return self.carousel.slides[tab_index]

    def get_main_tab(self):
        return self.carousel.slides[self.main_tab]

    def get_current_tab(self):
        return self.get_tab(self.carousel.slides.index(
            self.carousel.current_slide))

    def switch_tab(self, name_tab, search_by='text'):
        if search_by == 'name' and isinstance(name_tab, str):
            for tab_instance in self.tab_bar.parent.carousel.slides:
                if tab_instance.name == name_tab:
                    self.carousel.load_slide(tab_instance)
                    return
            raise ValueError(
                "switch_tab:\n\t"
                "name_tab not found in the tab list\n\t"
                f"search_by = {repr(search_by)} \n\t"
                f"name_tab = {repr(name_tab)} \n\t"
            )
        super().switch_tab(name_tab, search_by=search_by)

    def on_tab_switch(self, *args):
        tab = args[0]
        tab.on_pre_enter()
        super().on_tab_switch(self, *args)
