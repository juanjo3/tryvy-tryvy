#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.lang import Builder
from kivymd.uix.navigationdrawer import MDNavigationDrawer, MDNavigationLayout
from kivymd.uix.list import BaseListItem
from kivy.properties import ObjectProperty
import gettext
from functools import partial
_ = partial(gettext.dgettext, 'tryvy')

Builder.load_string('''
<TrytonNavLayout>
    id: nav_layout
    root_app: app
    nav_drawer: nav_drawer
    orientation: 'vertical'

    TrytonScreenManager:
        id: smanager

    TrytonNavDrawer:
        id: nav_drawer

<TrytonNavDrawer>
    BoxLayout:
        orientation: "vertical"

        FloatLayout:
            size_hint_y: None
            height: app.theme_cls.standard_increment

            canvas:
                Color:
                    rgba: app.theme_cls.primary_color
                Rectangle:
                    pos: self.pos
                    size: self.size

            BoxLayout:
                id: top_box
                size_hint_y: None
                height: app.theme_cls.standard_increment
                x: root.x
                pos_hint: {"top": 1}

                FitImage:
                    source: app.drawer_image

        ScrollView:
            pos_hint: {"top": 1}

            MDGridLayout:
                id: box_item
                cols: 1
                adaptive_height: True

                TrytonOneLineLeftIconItem:
                    icon: 'domain'
                    text: app.context.get('company.rec_name') or _('<Define company>')
                    on_release: app.create_many2one_company()
''')


class TrytonNavLayout(MDNavigationLayout):
    root_app = ObjectProperty(None)


class TrytonNavDrawer(MDNavigationDrawer):
    _default_items = 5

    def add_widget(self, widget, index=0):
        if issubclass(widget.__class__, BaseListItem):
            index = index or self._default_items
        super(TrytonNavDrawer, self).add_widget(widget, index)
