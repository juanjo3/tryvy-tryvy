# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import sys

__all__ = ['parse', 'get_options']

_options = {
    'dev': False,
    'persist_mode': 'LOCAL',
    'replication': True}
_parsed = False


def parse():
    global _parsed
    global _options
    if not _parsed:
        if '--persist-remote' in sys.argv:
            _options['persist_mode'] = 'REMOTE'
            sys.argv.remove('--persist-remote')
        if '--no-repl' in sys.argv:
            _options['replication'] = False
            sys.argv.remove('--no-repl')
        if '--dev' in sys.argv:
            _options['dev'] = True
            sys.argv.remove('--dev')
        _parsed = True
    return _options


def get_options():
    global _options
    return _options
