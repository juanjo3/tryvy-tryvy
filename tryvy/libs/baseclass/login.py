#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
from kivy.app import App
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.uix.screenmanager import Screen
from kivy.properties import BooleanProperty

__all__ = ['LoginScreen']

Builder.load_file(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', 'kv', 'login.kv')))


class LoginScreen(Screen):
    """Login screen. Opens when the application starts."""
    _first_time = True
    in_progress = BooleanProperty(False)

    def on_enter(self):
        if self._first_time:
            Clock.schedule_once(self.login, 0)
        self._first_time = False

    def login(self, dt):
        app = App.get_running_app()
        app.login()
