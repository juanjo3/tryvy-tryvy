#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
import json
from kivy.metrics import dp
from kivy.properties import (ObjectProperty, StringProperty, NumericProperty,
    BooleanProperty)
from kivy.lang import Builder
from kivy.compat import string_types
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import Screen
from kivymd.uix.dialog import MDDialog
from kivymd.uix.list import TwoLineListItem, MDList
from kivymd.uix.textfield import MDTextField
from kivymd.uix.button import MDFlatButton
import configparser
import traceback
import sys
import gettext
from ...filler_init import FillerInit
from ...ui.dialog import ErrorDialog

_ = gettext.gettext

DEFAULT_PORT = '8000'

__all__ = ['SettingsScreen']


Builder.load_file(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', 'kv', 'settings.kv')))

CONNECTION_KEYS = {'server', 'secondary_server', 'database', 'uid', 'pwd'}


class SettingMDItem(FillerInit):
    title = StringProperty('<No title set>')
    desc = StringProperty(None, allownone=True)
    disabled = BooleanProperty(False)
    section = StringProperty(None)
    key = StringProperty(None)
    value = ObjectProperty(None)
    panel = ObjectProperty(None)
    content = ObjectProperty(None)
    selected_alpha = NumericProperty(0)

    __events__ = ('on_release', )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.on_panel(self, kwargs['panel'])
        self.value = self.panel.get_value(self.section, self.key)

    def add_widget(self, *largs):
        if self.content is None:
            return super(SettingMDItem, self).add_widget(*largs)
        return self.content.add_widget(*largs)

    def on_value(self, instance, value):
        if not self.section or not self.key:
            return
        # get current value in config
        panel = self.panel
        if not isinstance(value, string_types):
            value = str(value)
        panel.set_value(self.section, self.key, value)


class SettingMDString(SettingMDItem, TwoLineListItem):
    dialog = ObjectProperty(None, allownone=True)
    textinput = ObjectProperty(None)

    def __init__(self, **kwargs):
        global _repl_interval
        self.password = kwargs.get('password') or False
        super(SettingMDString, self).__init__(**kwargs)
        self.text = _(self.title)
        self.input_type = kwargs.get('input_type')
        self.secondary_text = self.get_value()

    def get_value(self):
        if self.password and self.value:
            return "*" * len(self.value)
        else:
            return self.value or ''

    def on_panel(self, instance, value):
        if value is None:
            return
        self.fbind('on_release', self._create_popup)

    def on_value(self, instance, value):
        super(SettingMDString, self).on_value(instance, value)
        self.secondary_text = self.get_value()

    def _dismiss(self, *largs):
        if self.textinput:
            self.textinput.focus = False
        if self.dialog:
            self.dialog.dismiss()
        self.dialog = None

    def _validate(self, instance):
        self._dismiss()
        value = self.textinput.text.strip()
        self.value = value

    def _create_popup(self, instance):
        if (self.parent.parent.parent.parent.previous_screen != 'login'
                and self.key in CONNECTION_KEYS):
            # does not allow to edit connection from root screen
            error_dialog = ErrorDialog()
            error_dialog.open(title=_(self.title), height=350,
                text=_('Cannot edit connection parameters. Please logout.'))
            return
        self.textinput = textinput = MDTextField()
        textinput.text = self.value or ''
        textinput.font_style = 'Body1'
        textinput.theme_text_color = 'Secondary',
        textinput.password = self.password
        textinput.valign = 'top'
        if self.input_type == 'number':
            self.textinput.input_type = 'number'
            self.textinput.input_filter = 'int'
        textinput.bind(on_text_validate=self._validate)
        self.textinput = textinput
        layout = BoxLayout(
            orientation='vertical',
            spacing='12dp',
            size_hint_y=None)
        layout.add_widget(textinput)

        self.dialog = MDDialog(
            title=_(self.title),
            type='custom',
            content_cls=layout,
            size_hint=(.8, None),
            height=dp(220),
            auto_dismiss=False,
            buttons=[
                MDFlatButton(
                    text=_('Cancel'),
                    text_color=self.theme_cls.primary_color,
                    on_release=lambda *x: self._dismiss()),
                MDFlatButton(
                    text=_('OK'),
                    text_color=self.theme_cls.primary_color,
                    on_release=lambda *x: self._validate(None))
            ])
        self.dialog.open()


class SettingsScreen(Screen):
    config = ObjectProperty(None, allownone=True)
    app = ObjectProperty(None)
    _types = {
        'string': SettingMDString,
        'number': SettingMDString
    }
    previous_screen = StringProperty(None)
    config_values = [
        {
            "type": "string",
            "title": _('Main server (IP:port)'),
            "desc": _('Define Tryton server IP or name'),
            "section": "Settings",
            "key": "server"
        },
        {
            "type": "string",
            "title": _('Secondary server (IP:port)'),
            "desc": _('Define Tryton server IP or name'),
            "section": "Settings",
            "key": "secondary_server"
        },
        {
            "type": "string",
            "title": _('Tryton database'),
            "desc": _('Define database name'),
            "section": "Settings",
            "key": "database"
        },
        {
            "type": "string",
            "title": _('Tryton user'),
            "desc": _('Define Tryton user identifier'),
            "section": "Settings",
            "key": "uid"
        },
        {
            "type": "string",
            "title": _('Tryton user password'),
            "desc": _('Define Tryton user password'),
            "section": "Settings",
            "password": "True",
            "key": "pwd"
        },
        {
            "type": "number",
            "title": _('Frequency synchronization (s)'),
            "desc": _('Define frequency synchronization (s)'),
            "section": "Settings",
            "key": "repl_interval"
        }
    ]

    def on_go_back(self):
        self.parent.current = self.previous_screen

    def get_value(self, section, key):
        config = self.config
        if not config:
            return
        try:
            if key == 'server':
                val = config.get(section, key) + ':' + config.get(
                    section, 'port')
            elif key == 'secondary_server' and config.has_option(section, key):
                val = config.get(section, key) + ':' + config.get(
                    section, 'secondary_port')
            else:
                val = config.get(section, key)
        except configparser.NoOptionError:
            traceback.print_exc(file=sys.stdout)
            val = None
        return val or None

    def set_value(self, section, key, value):
        current = self.get_value(section, key)
        if current == value:
            return
        config = self.config

        def change_value(csection, ckey, cvalue):
            config.set(csection, ckey, cvalue)
            config.write()
            if self.app:
                self.app.on_config_change(config, csection, ckey, cvalue)

        if config:
            if (key == 'server' or key == 'secondary_server'):
                value_server, *value_port = value.split(':', 1)
                change_value(section, key, value_server)
                if value_port:
                    value_port, = value_port
                port = 'port' if key == 'server' else 'secondary_port'
                change_value(section, port, value_port or DEFAULT_PORT)
            else:
                change_value(section, key, value)

    def save_config(self, config, **kwargs):
        if not self.config:
            self.config = config
        for key, value in list(kwargs.items()):
            self.set_value('Settings', key, value)

    def create_data(self, config, filename=None):
        if self.ids.screen_container.children:
            return
        if not self.config:
            self.config = config

        data = self.config_values
        if filename is None and data is None:
            raise Exception(_('You must specify either the filename or data'))
        if filename is not None:
            with open(filename, 'r') as fd:
                data = json.loads(fd.read())
        if type(data) != list:
            raise ValueError(_('The first element must be a list'))
        panel = MDList()
        self.ids.screen_container.add_widget(panel)

        for setting in data:
            # determine the type and the class to use
            if 'type' not in setting:
                raise ValueError(_(
                    'One setting parameter is missing the "type" element'))
            ttype = setting['type']
            cls = self._types.get(ttype)
            if cls is None:
                message = (_('No class registered to handle the <%s> type') %
                    setting['type'])
                raise ValueError(message)

            # create a instance of the class, without the type attribute
            str_settings = {'input_type': setting['type']}
            del setting['type']
            for key, item in list(setting.items()):
                str_settings[str(key)] = item

            instance = cls(panel=self, **str_settings)

            # instance created, add to the panel
            panel.add_widget(instance)
