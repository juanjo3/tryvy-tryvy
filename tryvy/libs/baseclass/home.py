#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
from kivy.lang import Builder
from ...ui.screen import TrytonScreen

__all__ = ['HomeScreen']

Builder.load_file(os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..', 'kv', 'home.kv')))


class HomeScreen(TrytonScreen):
    """Home screen. Opens when the application log in."""

    def get_toolbar_actions(self):
        left = [['menu', lambda x:
            self.manager.parent.nav_drawer.set_state("toggle")]]
        right = []
        return left, right
