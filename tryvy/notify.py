# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from kivy.app import App
from kivy.utils import platform
from .osc import Response, Request
if platform == 'android':
    # bug: explicit instance due to does not work facade
    from plyer.platforms.android.notification import AndroidNotification
    notification = AndroidNotification()
else:
    from plyer import notification

__all__ = ['notify', 'push_notif', 'PopNotifResponse', 'PopNotifRequest']


_OSC_MSG_POP_NOTIF = b'/pop_notifications'

_notifications = []
_my_babel = {
    225: 'a',
    193: 'A',
    233: 'e',
    201: 'E',
    237: 'i',
    205: 'I',
    243: 'o',
    211: 'O',
    250: 'u',
    218: 'U'}


def notify(message, title=None):
    if not title:
        app = App.get_running_app()
        title = app.title
    if platform == 'android':
        msg = ''
        for c in message:
            msg += str(_my_babel.get(ord(c), c))
    else:
        msg = message

    kwargs = {
        'title': title,
        'message': msg,
        'toast': True,
        'app_icon': '../res/drawable/icon.png'}
    notification.notify(**kwargs)


def push_notif(message):
    _notifications.append(message
        if type(message) == str else message.encode('utf-8'))


def has_nofications():
    return bool(_notifications)


class PopNotifResponse(Response):
    message = _OSC_MSG_POP_NOTIF

    def message_callback(self, *args):
        global _notifications

        if args:
            for m in args:
                _notifications.remove(m)
        else:
            notifs = [n for n in _notifications] if _notifications else []
            self.send(data_array=notifs)


class PopNotifRequest(Request):
    message = _OSC_MSG_POP_NOTIF

    def message_callback(self, *args):
        if args:
            for m in args:
                notify(m)
            self.send(data_array=args, need_response=False)
