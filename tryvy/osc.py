# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from functools import partial
import sys
import traceback
from oscpy.server import OSCThreadServer
from kivy.clock import Clock
from time import sleep
import gettext

_ = gettext.gettext

__all__ = ['MessageUniqueError', 'Request', 'Response', 'Reader']


class UsedListenPortError(Exception):
    '''Used listen port error'''

    def __init__(self, port):
        message = _('Port %s is used by another OSCReader instance') % port
        super(MessageUniqueError, self).__init__(message)


class MessageUniqueError(Exception):
    '''Message unique error'''

    def __init__(self, Message):
        message = _('OSC message %s can be binded only once') % Message.message
        super(MessageUniqueError, self).__init__(message)


class NotBindedMessageError(Exception):
    '''Not binded message error'''

    def __init__(self, Message):
        message = _('OSC message %s is not binded') % Message.message
        super(NotBindedMessageError, self).__init__(message)


class Response(object):
    '''OSC Request'''
    message = None

    def __init__(self, sender):
        self._sender = sender

    def msg_callback_wrapper(self, *args):
        try:
            self.message_callback(*args)
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            raise e

    def message_callback(*args):
        raise NotImplementedError

    def send(self, data_array=[]):
        self._sender(self.message, values=data_array)


class Request(Response):
    '''OSC Request'''

    def __init__(self, sender):
        super(Request, self).__init__(sender)
        self._is_response = True

    def msg_callback_wrapper(self, *args):
        self._is_response = True
        super().msg_callback_wrapper(*args)

    def is_response(self):
        return self._is_response

    def send(self, data_array=[], force=False, need_response=True):
        if self.is_response() or force:
            self._is_response = not need_response
            super(Request, self).send(data_array)


class Reader(object):
    '''OSC Reader'''

    # TODO: Method to Close OSCThreatServer

    _listened_ports = []

    def __init__(self, listen_port, send_port, interval=None):
        if listen_port in self._listened_ports:
            raise UsedListenPortError(listen_port)
        self._osc = OSCThreadServer(encoding='utf8')
        self._listened_ports.append(listen_port)
        self._listen_port = listen_port
        self._sender = partial(self._osc.send_message, values=[],
            ip_address='localhost', port=send_port)
        self._messages = {}
        self._responses = {}
        self._requests = {}
        self._response_methods = []
        self._osc.listen(address='localhost', port=listen_port, default=True)
        self._interval = interval

    def __del__(self):
        self._listened_ports.remove(self._listen_port)

    def _message_bind(self, Message):
        if Message.message not in self._messages:
            msg = Message(self._sender)
            self._messages[Message.message] = msg
            self._osc.bind(Message.message, msg.msg_callback_wrapper)
            return msg
        else:
            raise MessageUniqueError(Message)

    def _message_unbind(self, Message):
        if Message.message in self._messages:
            self._osc.unbind(Message.message, Message.msg_callback_wrapper)
            return self._messages.pop(Message.message)
        return None

    def response_bind(self, Response):
        response = self._message_bind(Response)
        self._responses[Response.message] = response
        return response

    def response_unbind(self, Response):
        if self._message_unbind(Response):
            del self._responses[Response.message]

    def request_append(self, Request):
        request = self._message_bind(Request)
        self._requests[Request.message] = request
        self._response_methods.append(request.is_response)
        return request

    def request_remove(self, Request):
        request = self._message_unbind(Request)
        if request:
            self._response_methods.remove(request.is_response)
            self._requests.remove(request)

    def _are_response(self):
        return all([m() for m in self._response_methods])

    def wait_for_response(self, Request, data_array=[], need_response=True):
        request = self._messages.get(Request.message, None)
        must_create = request is None
        if must_create:
            request = Request(self._sender)
            if need_response:
                self._osc.bind(Request.message, request.msg_callback_wrapper)
        request.send(data_array, need_response=need_response)
        while not request.is_response():
            sleep(self._interval)

        if must_create and need_response:
            self._osc.unbind(Request.message, request.msg_callback_wrapper)

    def _send_requests(self, interval=None):
        if self._are_response():
            for rq in self._requests.values():
                rq.send()

    def schedule_requests(self, interval=None):
        interval = interval or self._interval
        assert interval
        Clock.schedule_interval(self._send_requests, 2 * interval)
