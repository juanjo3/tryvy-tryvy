# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import sys
import os
import gzip
import traceback
from configparser import ConfigParser
from kivy.utils import platform
from kivy.clock import Clock
from shutil import copyfile
from trytond.config import config
from ..commandline import get_options
import io

__all__ = ['setup_tryton']

IGNORE_REPOS = ['trytond', 'tryton', 'sao', 'proteus',
                'dvconfig', 'config', 'nereid']


def _get_modules_path():
    end_str = ('/site-packages' if not get_options().get('dev', False)
        else'/trytond')
    mod_path = None
    mod_init = None
    for p in sys.path:
        if (p.endswith(end_str)):
            mod_init = p + '/trytond/modules/__init__'
            break
    for f_ext in ('py', 'pyc', 'pyo'):
        mod_path = '%s.%s' % (mod_init, f_ext)
        if os.path.exists(mod_path):
            return os.path.dirname(mod_path)
    return None


def _create_links(dev=False):
    res = False or (platform != 'androd')
    trytond_mod_path = _get_modules_path()
    if not trytond_mod_path:
        raise Exception('Tryton modules path not found')
    print('Trytond modules path: %s' % trytond_mod_path)
    work_dir_path = os.getcwd()
    print('Working folder path: %s' % work_dir_path)

    cp = ConfigParser()
    cp.read('config/own.cfg', encoding='utf8')
    repos_ = cp.sections()
    for sect in repos_:
        if sect in IGNORE_REPOS:
            continue
        dest_path = '%s/%s' % (trytond_mod_path, sect)
        src_path = '%s/%s/%s' % (work_dir_path, cp.get(sect, 'path'), sect)
        if not os.path.exists(dest_path):
            print(('Creating symbolic link of "%s"' % sect))
            if os.path.exists(src_path):
                print(os.popen('ln -s -f %s %s' % (src_path,
                    trytond_mod_path)).read())
                res = True
            else:
                raise Exception('Source code not found for module %s' % sect)

    return res


def _get_repos():
    cp = ConfigParser()
    cp.read('config/own.cfg', encoding='utf8')
    return cp.sections()


# TODO: Must run in backgroud
def _update_db(repl_database):
    return
    import trytond.commandline as commandline
    from trytond.config import config
    import trytond.admin as admin
    parser = commandline.get_parser_admin()
    options = parser.parse_args()
    options.configfile = 'etc/trytond.conf'
    config.update_etc(options.configfile)
    options.database_names.append(repl_database)
    modules = _get_repos()
    print('Installing modules: %s' % modules)
    options.update.extend(modules)
    admin.run(options)


def download_db(app, dest_path):
    from trytond.config import config
    from cryptography.fernet import Fernet
    from tryton_rpc import Session

    fernet_key = config.get('replication', 'fernet_key')
    if not fernet_key:
        raise Exception('Missing Fernet key on replication section.')
    fernet = Fernet(fernet_key)
    encrypted_pwd = fernet.encrypt(app.pwd.encode('utf-8'))
    session = Session()
    for x in range(0, 2):
        try:
            server, port = app.server, app.port
            if x > 0:
                server, port = app.secondary_server, app.secondary_port
            session.login(app.uid, app.pwd, server, port, app.database)
            break
        except Exception:
            if x > 0:
                session = None
            traceback.print_exc(file=sys.stdout)
    if not session:
        return app.replication_error()
    try:
        gzip_content = session.execute('model', 'res.user',
            'download_replica_db', app.repl_database, encrypted_pwd)
    except Exception:
        return app.replication_error()
    content = gzip.decompress(gzip_content)
    with open(dest_path, 'wb') as f:
        f.write(content)
    # enter on app
    app.replication_finished([])


def _check_db(app, repl_database):
    """
    Check database.
    Returns:
        db_init: determines if db has been initialized
        downloading: determines if db is being downloaded in another thread
    """
    def must_init_db():
        if platform == 'android':
            dbmanage = '%s/dbmanage' % work_dir_path
            try:
                with io.open(dbmanage, 'r') as f:
                    lines = f.readlines()
            except IOError:
                with io.open(dbmanage, 'a') as f:
                    f.write(str('#list of databases to manage' + '\n'))
                    f.write(str(repl_database) + '\n')
                return False
            lines = lines[1:]
            if str(repl_database + '\n') not in lines:
                print("Must init database %s" % repl_database)
                with io.open(dbmanage, 'a') as f:
                    f.write(str(repl_database) + '\n')
                return True
        return False

    db_paths = get_db_file_paths(app.name, repl_database)
    work_dir_path = db_paths[1]
    if must_init_db() or not os.path.exists(db_paths[0]):
        dest_path = db_paths[0]
        if platform != 'android':
            print('Copy %s.sqlite from template folder ...' % app.repl_database)
            template_db_file = '%s/template/%s.sqlite' % (
                work_dir_path, app.repl_database)
            copyfile(template_db_file, dest_path)
            return True, False
        else:
            Clock.schedule_once(lambda dt: download_db(app, dest_path))
            return True, True
    return False, False


def get_db_file_paths(_app_name, repl_database):
    work_dir_path = os.getcwd()
    if platform == 'android':
        from android.storage import app_storage_path
        db_path = app_storage_path()
        if not os.path.exists(db_path):
            os.mkdir(db_path)
        db_path = os.path.join(db_path, 'data')
        if not os.path.exists(db_path):
            os.mkdir(db_path)
    else:
        db_path = config.get('database', 'path')
        if not os.path.exists(db_path):
            os.mkdir(db_path)
    return (os.path.join(db_path, '%s.sqlite' % repl_database),
            work_dir_path, db_path)


def setup_tryton(app, repl_database='tryton'):
    print('Search for new modules ...')
    new_links = _create_links()
    new_db, downloading = _check_db(app, repl_database)
    if new_links and not new_db:
        _update_db(repl_database)
    return not new_db, downloading
