# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
import gettext
import sys
from .misc import get_default_language

_ = gettext.gettext


class ConfigManager(object):
    "Config manager"

    def __init__(self):
        self.defaults = {
            'client.lang': get_default_language(),
            'client.language_direction': 'ltr',
            'client.limit': 1000,
        }
        self.config = {}
        self.options = {
            'login.host': True
        }
        self.arguments = []

    def __setitem__(self, key, value, config=True):
        self.options[key] = value
        if config:
            self.config[key] = value

    def __getitem__(self, key):
        return self.options.get(key, self.config.get(key,
            self.defaults.get(key)))

    def save(self):
        pass


CONFIG = ConfigManager()

CURRENT_DIR = os.path.dirname(__file__)
if (os.name == 'nt' and hasattr(sys, 'frozen') and
        os.path.basename(sys.executable) == 'tryvy.exe'):
    CURRENT_DIR = os.path.dirname(sys.executable)
elif (os.name == 'mac' or
        (hasattr(os, 'uname') and os.uname()[0] == 'Darwin')):
    resources = os.path.join(os.path.dirname(sys.argv[0]), '..', 'Resources')
    if os.path.isdir(resources):
        CURRENT_DIR = resources
