#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
import sys
import re
import time
import tempfile
import subprocess
import unicodedata
import dateutil.tz
import gettext
from functools import partial
from kivy.properties import DictProperty, AliasProperty
from kivy.utils import platform
from ..tryton.call import CONTEXT, execute as call_execute
if platform == 'android':
    from jnius import autoclass

_ = partial(gettext.dgettext, 'tryvy')

LOCAL_TIME_ZONE = ''

DEFAULT_PRINTER_DEVICE = None


class ModelAccess(object):
    _permissions = DictProperty({
        'create': False,
        'delete': False,
        'modify': False
    })

    def get_perm_create(self):
        return self._permissions['create']

    def set_perm_create(self, value):
        self._permissions['create'] = value

    perm_create = AliasProperty(get_perm_create, set_perm_create,
        bind=['_permissions'])

    def get_perm_delete(self):
        return self._permissions['delete']

    def set_perm_delete(self, value):
        self._permissions['delete'] = value

    perm_delete = AliasProperty(get_perm_delete, set_perm_delete,
        bind=['_permissions'])

    def get_perm_modify(self):
        return self._permissions['modify']

    def set_perm_modify(self, value):
        self._permissions['modify'] = value

    perm_modify = AliasProperty(get_perm_modify, set_perm_modify,
        bind=['_permissions'])


def _set_time():
    if hasattr(time, 'tzset'):
        time.tzset()


def settz():
    global LOCAL_TIME_ZONE
    try:
        os.environ.pop('TZ', None)
        _set_time()
        from time import tzname
        LOCAL_TIME_ZONE = tzname[0]
    finally:
        os.environ['TZ'] = 'UTC'
        _set_time()


def timezoned_date(date, reverse=False):
    lzone = dateutil.tz.gettz(LOCAL_TIME_ZONE)
    szone = dateutil.tz.tzutc()
    if reverse:
        lzone, szone = szone, lzone
    return date.replace(tzinfo=szone).astimezone(lzone).replace(tzinfo=None)


def untimezoned_date(date):
    return timezoned_date(date, reverse=True).replace(tzinfo=None)


_slugify_strip_re = re.compile(r'[^\w\s-]')
_slugify_hyphenate_re = re.compile(r'[-\s]+')


def slugify(value):
    if not isinstance(value, str):
        value = str(value)
    value = unicodedata.normalize('NFKD', value)
    value = str(_slugify_strip_re.sub('', value).strip())
    return _slugify_hyphenate_re.sub('-', value)


def file_write(filename, data):
    if isinstance(data, str):
        data = data.encode('utf-8')
    dtemp = tempfile.mkdtemp(prefix='tryton_')
    if not isinstance(filename, str):
        name, ext = filename
    else:
        name, ext = os.path.splitext(filename)
    filename = ''.join([slugify(name), os.extsep, slugify(ext)])
    filepath = os.path.join(dtemp, filename)
    with open(filepath, 'wb') as fp:
        fp.write(data)
    return filepath


def file_open(filename, type=None, print_p=False):
    global DEFAULT_PRINTER_DEVICE
    if os.name == 'nt':
        operation = 'open'
        if print_p:
            operation = 'print'
        os.startfile(os.path.normpath(filename), operation)
    elif sys.platform == 'darwin':
        subprocess.Popen(['/usr/bin/open', filename])
    elif platform == 'android':
        BluetoothAdapter = autoclass('android.bluetooth.BluetoothAdapter')
        UUID = autoclass('java.util.UUID')
        adapter = BluetoothAdapter.getDefaultAdapter()

        if not adapter or not adapter.isEnabled():
            return

        paired_devices = BluetoothAdapter.getDefaultAdapter(
            ).getBondedDevices().toArray()
        socket = None
        printer_mask = 0b000001000000011010000000
        for device in paired_devices:
            # todo: ask if many printers
            if (device.getBluetoothClass().hashCode() & printer_mask
                    ) != printer_mask:
                # only printer devices
                continue

            if (DEFAULT_PRINTER_DEVICE
                    and DEFAULT_PRINTER_DEVICE != device.getName()):
                continue

            socket = device.createRfcommSocketToServiceRecord(
                UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"))
            if not socket:
                continue
            send_stream = socket.getOutputStream()
            try:
                # todo: check if active (the only way is scanning)
                socket.connect()
                DEFAULT_PRINTER_DEVICE = device.getName()
                break
            except Exception:
                socket = None
                if DEFAULT_PRINTER_DEVICE == device.getName():
                    DEFAULT_PRINTER_DEVICE = None
                continue

        if not socket:
            DEFAULT_PRINTER_DEVICE = None
            raise Exception(_('Cannot find an active printer.'))
        try:
            # TODO: do not save file
            with open(filename, 'rb') as f:
                send_stream.write(f.read())
        finally:
            send_stream.flush()
            socket.close()
            os.remove(filename)
    else:
        subprocess.Popen(['xdg-open', filename])


def file_print(type, data, name):
    fp_name = file_write((name, type), data)
    file_open(fp_name, type, print_p=True)


def ContextReload(callback=None):
    def update(context):
        CONTEXT.clear()
        if context:
            CONTEXT.update(context)
        if callback:
            callback()

    call_execute('model', 'res.user', 'get_preferences', True,
        callback=update)
